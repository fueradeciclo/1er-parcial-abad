﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _1er_Parcial
{
    public partial class Listar_Recibo : Form
    {
        public Listar_Recibo()
        {
            InitializeComponent();
            this.WindowState = FormWindowState.Maximized;
            dataGridView1.ClearSelection();
            Enlazar();
        }

        BLL.Empleado BLLEmp = new BLL.Empleado();
        BLL.Recibo BLLRec = new BLL.Recibo();

        public void Enlazar()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = BLLEmp.Listar();
            dataGridView2.DataSource = null;
            dataGridView2.DataSource = BLLRec.Listar();
        }



        private void btntod_Click(object sender, EventArgs e)
        {

            List<BE.Recibo> lista = BLLRec.Listar();
            var l = (from BE.Recibo Rec in lista
                     where Rec.Legajo == int.Parse(dataGridView1.SelectedRows[0].Cells[0].Value.ToString())
                    select Rec
                     );

            dataGridView2.DataSource = null;
            dataGridView2.DataSource = l.ToList();

        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnmes_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtMes.Text) || string.IsNullOrWhiteSpace(txtanio.Text))
            {
                MessageBox.Show("El Mes y el Año deben contener valores validos");
            }
            else if (int.Parse(txtMes.Text) >= 1 && int.Parse(txtMes.Text) <= 12)
            {
            
            List<BE.Recibo> lista = BLLRec.Listar();
            var l = (from BE.Recibo Rec in lista
                     where Rec.Legajo == int.Parse(dataGridView1.SelectedRows[0].Cells[0].Value.ToString()) &&
                           Rec.Mes == int.Parse(txtMes.Text) &&
                           Rec.Anio == int.Parse(txtanio.Text)
                     select Rec
                     );

            dataGridView2.DataSource = null;
            dataGridView2.DataSource = l.ToList();
            }
            else
            {
                MessageBox.Show("El Mes debe contener valores entre 1 y 12");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Enlazar();
        }

        private void validarListado(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
              if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}
