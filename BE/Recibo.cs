﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Recibo
    {
        private int legajo;

        public int Legajo
        {
            get { return legajo; }
            set { legajo = value; }
        }

        private int  mes;

        public int Mes
        {
            get { return mes; }
            set { mes = value; }
        }

        private int anio;

        public int Anio
        {
            get { return anio; }
            set { anio = value; }
        }

        private float sdo_bruto;

        public float Sdo_bruto
        {
            get { return sdo_bruto; }
            set { sdo_bruto = value; }
        }

        private float sdo_neto;

        public float Sdo_neto
        {
            get { return sdo_neto; }
            set { sdo_neto = value; }
        }

        private float tot_dtos;

        public float Tot_dtos
        {
            get { return tot_dtos; }
            set { tot_dtos = value; }
        }

        private List<Concepto> list_con;

        public List<Concepto> List_con
        {
            get { return list_con; }
            set { list_con = value; }
        }

    }
}
