﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _1er_Parcial
{
    public partial class ABM_Concepto : Form
    {
        public ABM_Concepto()
        {
            InitializeComponent();
            Enlazar();
        }

        BE.Concepto Conc = new BE.Concepto();
        BLL.Concepto BLLCon = new BLL.Concepto();
        

        public void Enlazar()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = BLLCon.Listar();
            comboBox1.DataSource = Enum.GetValues(typeof(Signo));
        }

        private void btnAlta_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrWhiteSpace(txtDes.Text) || string.IsNullOrWhiteSpace(txtPor.Text))
            {
                MessageBox.Show("Los campos Descripción y Porcentaje deben contener datos válidos");
            }
            else
            {
                if (int.Parse(txtPor.Text) >= 1 && int.Parse(txtPor.Text) <= 99)
                {
                    Conc.Descripcion = txtDes.Text;
                    Conc.Porcentaje = float.Parse(txtPor.Text);
                    Conc.Num_calculo = float.Parse((Conc.Porcentaje / 100).ToString());
                    Conc.Tipo_concepto = comboBox1.Text;
                    BLLCon.Insertar(Conc);
                    Enlazar();
                }
                else
                {
                    MessageBox.Show("El Porcentaje del Concepto debe estar entre 1 y 99");
                }
            }
            

        }

        private void btnBaja_Click(object sender, EventArgs e)
        {
            BE.Concepto Conc = (BE.Concepto)dataGridView1.SelectedRows[0].DataBoundItem;
            BLLCon.Borrar(Conc);
            Enlazar();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            txtDes.Text = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
            txtPor.Text = dataGridView1.SelectedRows[0].Cells[2].Value.ToString();
            comboBox1.Text = dataGridView1.SelectedRows[0].Cells[4].Value.ToString();
        }

        private void btnMod_Click(object sender, EventArgs e)
        {
            BE.Concepto Conc = (BE.Concepto)dataGridView1.SelectedRows[0].DataBoundItem;
            Conc.Descripcion = txtDes.Text;
            Conc.Porcentaje = float.Parse(txtPor.Text);
            Conc.Num_calculo = float.Parse((Conc.Porcentaje / 100).ToString());
            Conc.Tipo_concepto = comboBox1.Text;
            BLLCon.Editar(Conc);
            Enlazar();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ValidarPorc(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
              if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}
