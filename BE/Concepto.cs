﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Concepto
    {
        private int id_con;

        public int Id_con
        {
            get { return id_con; }
            set { id_con = value; }
        }

        private string descripcion;

        public string Descripcion
        {
            get { return descripcion; }
            set { descripcion = value; }
        }

        private float porcentaje;

        public float Porcentaje
        {
            get { return porcentaje; }
            set { porcentaje = value; }
        }

        private float num_calculo;

        public float Num_calculo
        {
            get { return num_calculo; }
            set { num_calculo = value; }
        }


        private string tipo_concepto;

        public string Tipo_concepto
        {
            get { return tipo_concepto; }
            set { tipo_concepto = value; }
        }


    }
}
