﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _1er_Parcial
{
    public partial class ABM_Empleado : Form
    {
        public ABM_Empleado()
        {
            InitializeComponent();
            Enlazar();
        }

        BE.Empleado Emp = new BE.Empleado();
        BLL.Empleado BLLemp = new BLL.Empleado();

        public void Enlazar()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = BLLemp.Listar();
        }


        private void btnAlta_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrWhiteSpace(txtNom.Text) || string.IsNullOrWhiteSpace(txtApe.Text) || string.IsNullOrWhiteSpace(txtCUIL.Text))
            {
                MessageBox.Show("Los campos Nombre, Apellido y CUIL deben contener datos válidos");
            }
            else
            {
                Emp.Nombre = txtNom.Text;
                Emp.Apellido = txtNom.Text;
                Emp.Cuil = txtCUIL.Text;
                Emp.Fec_alta = dateTimePicker1.Value;
                BLLemp.Insertar(Emp);
                Enlazar();
            }
        }

        private void btnBaja_Click(object sender, EventArgs e)
        {
            BE.Empleado Emp = (BE.Empleado)dataGridView1.SelectedRows[0].DataBoundItem;
            BLLemp.Borrar(Emp);
            Enlazar();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            txtNom.Text = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
            txtApe.Text = dataGridView1.SelectedRows[0].Cells[2].Value.ToString();
            txtCUIL.Text = dataGridView1.SelectedRows[0].Cells[3].Value.ToString();
        }

        private void btnMod_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtNom.Text) || string.IsNullOrWhiteSpace(txtApe.Text) || string.IsNullOrWhiteSpace(txtCUIL.Text))
            {
                MessageBox.Show("Los campos Nombre, Apellido y CUIL deben contener datos válidos");
            }
            else
            {
                BE.Empleado Emp = (BE.Empleado)dataGridView1.SelectedRows[0].DataBoundItem;
                Emp.Nombre = txtNom.Text;
                Emp.Apellido = txtApe.Text;
                Emp.Cuil = txtCUIL.Text;
                Emp.Fec_alta = dateTimePicker1.Value;

                BLLemp.Editar(Emp);
                Enlazar();
            }
            
        }

        private void btnSal_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void validarCUIL(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
              if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}