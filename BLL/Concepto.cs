﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Concepto
    {
        DAL.MP_Concepto mp = new DAL.MP_Concepto();

        public int Insertar(BE.Concepto concepto)
        {

            return mp.Insertar(concepto);
        }

        public int Editar(BE.Concepto concepto)
            
            {
                return mp.Editar(concepto);
            }
            
        

        public int Borrar(BE.Concepto concepto)
        {
            return mp.Borrar(concepto);
        }

        public List<BE.Concepto> Listar()
        {
            return mp.Listar();
        }


    }
}
