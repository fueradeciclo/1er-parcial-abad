﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Recibo
    {
        DAL.MP_Recibo mp = new DAL.MP_Recibo();

        public int Insertar(BE.Recibo recibo)
        {
            return mp.Insertar(recibo);
        }

        public List<BE.Recibo> Listar()
        {
            return mp.Listar();
        }
    }
}
