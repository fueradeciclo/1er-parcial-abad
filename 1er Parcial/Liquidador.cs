﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _1er_Parcial
{
    public partial class Liquidador : Form
    {
        public Liquidador()
        {
            InitializeComponent();
        }

        private void aBMEmpleadoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ABM_Empleado frm = new ABM_Empleado();
            frm.MdiParent = this;
            frm.Show();
        }

        private void aBMConceptoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ABM_Concepto frm = new ABM_Concepto();
            frm.MdiParent = this;
            frm.Show();
        }

        private void altaReciboToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Alta_Recibo frm = new Alta_Recibo();
            frm.MdiParent = this;
            frm.Show();
        }

        private void listadoDeRecibosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Listar_Recibo frm = new Listar_Recibo();
            frm.MdiParent = this;
            frm.Show();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
