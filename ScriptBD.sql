USE [master]
GO
/****** Object:  Database [1er_Parcial]    Script Date: 30/9/2020 10:27:50 ******/
CREATE DATABASE [1er_Parcial]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'1er_Parcial', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\1er_Parcial.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'1er_Parcial_log', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\1er_Parcial_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [1er_Parcial] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [1er_Parcial].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [1er_Parcial] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [1er_Parcial] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [1er_Parcial] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [1er_Parcial] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [1er_Parcial] SET ARITHABORT OFF 
GO
ALTER DATABASE [1er_Parcial] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [1er_Parcial] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [1er_Parcial] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [1er_Parcial] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [1er_Parcial] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [1er_Parcial] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [1er_Parcial] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [1er_Parcial] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [1er_Parcial] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [1er_Parcial] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [1er_Parcial] SET  DISABLE_BROKER 
GO
ALTER DATABASE [1er_Parcial] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [1er_Parcial] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [1er_Parcial] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [1er_Parcial] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [1er_Parcial] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [1er_Parcial] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [1er_Parcial] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [1er_Parcial] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [1er_Parcial] SET  MULTI_USER 
GO
ALTER DATABASE [1er_Parcial] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [1er_Parcial] SET DB_CHAINING OFF 
GO
ALTER DATABASE [1er_Parcial] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [1er_Parcial] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [1er_Parcial]
GO
/****** Object:  StoredProcedure [dbo].[Concepto_Borrar]    Script Date: 30/9/2020 10:27:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Concepto_Borrar]
@ID int
as
begin

	delete from Concepto
	where ID_Concepto = @ID
	
end


GO
/****** Object:  StoredProcedure [dbo].[Concepto_Editar]    Script Date: 30/9/2020 10:27:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Concepto_Editar] 
@ID int, @des Varchar (50), @por float,@ncal decimal (8,4), @Tip Varchar (50)

as
begin

	update Concepto
	set
	Descripcion = @des,
	Porcentaje = @por,
	Num_Calculo = @ncal,
	Tipo_Con = @Tip

	where ID_Concepto = @ID
	
end


GO
/****** Object:  StoredProcedure [dbo].[Concepto_Insertar]    Script Date: 30/9/2020 10:27:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Concepto_Insertar]
@des Varchar (50), @por int , @ncal decimal(8, 4), @Tip Varchar (50)
as
begin

	declare @ID int

	set @ID = ISNULL((Select max(ID_Concepto) from Concepto),0) +1

	insert into Concepto (ID_Concepto,Descripcion,Porcentaje,Num_Calculo,Tipo_Con)
	values (@ID,@des,@por,@ncal,@Tip)

end


GO
/****** Object:  StoredProcedure [dbo].[Concepto_Listar]    Script Date: 30/9/2020 10:27:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Concepto_Listar]
as
begin

SELECT c.ID_Concepto , c.Descripcion , c.Porcentaje, c.Num_Calculo , c.Tipo_Con
FROM Concepto c

end


GO
/****** Object:  StoredProcedure [dbo].[Empleado_Borrar]    Script Date: 30/9/2020 10:27:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[Empleado_Borrar]
@leg int
as
begin

	delete from Empleado
	where Legajo = @leg
	
end


GO
/****** Object:  StoredProcedure [dbo].[Empleado_Editar]    Script Date: 30/9/2020 10:27:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[Empleado_Editar] 
@leg int, @nom Varchar(50), @ape Varchar (50), @cuil Varchar (50), @fec_alta Date
as
begin

	update Empleado
	set
	Nombre = @nom,
	Apellido = @ape,
	CUIL = @cuil,
	Fecha_Alta = @fec_alta

	where Legajo = @leg
	
end


GO
/****** Object:  StoredProcedure [dbo].[Empleado_Insertar]    Script Date: 30/9/2020 10:27:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[Empleado_Insertar]
@nom Varchar(50), @ape Varchar (50), @cuil Varchar (50), @fec_alta Date
as
begin

	declare @leg int

	set @leg = ISNULL((Select max(Legajo) from Empleado),0) +1

	insert into Empleado (Legajo,Nombre,Apellido,CUIL,Fecha_Alta)
	values (@leg,@nom,@ape,@cuil,@fec_alta)

end


GO
/****** Object:  StoredProcedure [dbo].[Empleado_Listar]    Script Date: 30/9/2020 10:27:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Empleado_Listar]
as
begin

SELECT e.Legajo , e.Nombre , e.Apellido , e.CUIL , e.Fecha_Alta
FROM Empleado e

end


GO
/****** Object:  StoredProcedure [dbo].[Recibo_Insertar]    Script Date: 30/9/2020 10:27:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[Recibo_Insertar]
@leg int , @mes int , @anio int , @sbru float , @snet float, @tdes float
as
begin

	insert into Recibo (Legajo_Emp,Mes,Anio,Sueldo_Bruto,Sueldo_Neto,Total_Descuentos)
	values (@leg,@mes,@anio,@sbru,@snet,@tdes)

end


GO
/****** Object:  StoredProcedure [dbo].[Recibo_Listar]    Script Date: 30/9/2020 10:27:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[Recibo_Listar]
as
begin

SELECT r.Legajo_Emp , r.Mes , r.Anio , r.Sueldo_Bruto , r.Sueldo_Neto , r.Total_Descuentos
FROM Recibo r

end


GO
/****** Object:  Table [dbo].[Concepto]    Script Date: 30/9/2020 10:27:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Concepto](
	[ID_Concepto] [int] NULL,
	[Descripcion] [varchar](50) NULL,
	[Porcentaje] [float] NULL,
	[Num_Calculo] [float] NULL,
	[Tipo_Con] [varchar](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Empleado]    Script Date: 30/9/2020 10:27:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Empleado](
	[Legajo] [int] NOT NULL,
	[Nombre] [varchar](50) NULL,
	[Apellido] [varchar](50) NULL,
	[CUIL] [varchar](50) NULL,
	[Fecha_Alta] [date] NULL,
 CONSTRAINT [PK_Empleado] PRIMARY KEY CLUSTERED 
(
	[Legajo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Recibo]    Script Date: 30/9/2020 10:27:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Recibo](
	[Legajo_Emp] [int] NULL,
	[Mes] [int] NULL,
	[Anio] [int] NULL,
	[Sueldo_Bruto] [float] NULL,
	[Sueldo_Neto] [float] NULL,
	[Total_Descuentos] [float] NULL
) ON [PRIMARY]

GO
USE [master]
GO
ALTER DATABASE [1er_Parcial] SET  READ_WRITE 
GO
