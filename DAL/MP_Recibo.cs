﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class MP_Recibo
    {
        private Acceso acceso = new Acceso();

        public List<BE.Recibo> Listar()
        {
            DataTable tabla = acceso.Leer("Recibo_Listar", null);
            List<BE.Recibo> recibos = new List<BE.Recibo>();

            foreach (DataRow registro in tabla.Rows)
            {
                BE.Recibo recibo = new BE.Recibo();

                recibo.Legajo = int.Parse(registro["Legajo_Emp"].ToString());
                recibo.Mes = int.Parse(registro["Mes"].ToString());
                recibo.Anio = int.Parse(registro["Anio"].ToString());
                recibo.Sdo_bruto = float.Parse(registro["Sueldo_Bruto"].ToString());
                recibo.Sdo_neto = float.Parse(registro["Sueldo_Neto"].ToString());
                recibo.Tot_dtos = float.Parse(registro["Total_Descuentos"].ToString());
                

                recibos.Add(recibo);
            }

            return recibos;
        }

        public int Insertar(BE.Recibo recibo)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametro("@leg", recibo.Legajo));
            parametros.Add(acceso.CrearParametro("@mes", recibo.Mes));
            parametros.Add(acceso.CrearParametro("@anio", recibo.Anio));
            parametros.Add(acceso.CrearParametro("@sbru", recibo.Sdo_bruto));
            parametros.Add(acceso.CrearParametro("@snet", recibo.Sdo_neto));
            parametros.Add(acceso.CrearParametro("@tdes", recibo.Tot_dtos));

            return acceso.Escribir("Recibo_Insertar", parametros);
        }
    
    }
}
