﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Empleado
    {
        DAL.MP_Empleado mp = new DAL.MP_Empleado();

        public int Insertar(BE.Empleado empleado)
        {
            return mp.Insertar(empleado); 
        }

        public int Editar(BE.Empleado empleado)
        {
            return mp.Editar(empleado);
        }

        public int Borrar(BE.Empleado empleado)
        {
            return mp.Borrar(empleado);
        }

        public List<BE.Empleado> Listar()
        {
            return mp.Listar();
        }

    }


}
