﻿namespace _1er_Parcial
{
    partial class Liquidador
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.aBMEmpleadoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aBMConceptoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.altaReciboToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listadoDeRecibosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.aBMEmpleadoToolStripMenuItem,
            this.aBMConceptoToolStripMenuItem,
            this.altaReciboToolStripMenuItem,
            this.listadoDeRecibosToolStripMenuItem,
            this.salirToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1419, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(12, 20);
            // 
            // aBMEmpleadoToolStripMenuItem
            // 
            this.aBMEmpleadoToolStripMenuItem.Name = "aBMEmpleadoToolStripMenuItem";
            this.aBMEmpleadoToolStripMenuItem.Size = new System.Drawing.Size(101, 20);
            this.aBMEmpleadoToolStripMenuItem.Text = "ABM Empleado";
            this.aBMEmpleadoToolStripMenuItem.Click += new System.EventHandler(this.aBMEmpleadoToolStripMenuItem_Click);
            // 
            // aBMConceptoToolStripMenuItem
            // 
            this.aBMConceptoToolStripMenuItem.Name = "aBMConceptoToolStripMenuItem";
            this.aBMConceptoToolStripMenuItem.Size = new System.Drawing.Size(100, 20);
            this.aBMConceptoToolStripMenuItem.Text = "ABM Concepto";
            this.aBMConceptoToolStripMenuItem.Click += new System.EventHandler(this.aBMConceptoToolStripMenuItem_Click);
            // 
            // altaReciboToolStripMenuItem
            // 
            this.altaReciboToolStripMenuItem.Name = "altaReciboToolStripMenuItem";
            this.altaReciboToolStripMenuItem.Size = new System.Drawing.Size(79, 20);
            this.altaReciboToolStripMenuItem.Text = "Alta Recibo";
            this.altaReciboToolStripMenuItem.Click += new System.EventHandler(this.altaReciboToolStripMenuItem_Click);
            // 
            // listadoDeRecibosToolStripMenuItem
            // 
            this.listadoDeRecibosToolStripMenuItem.Name = "listadoDeRecibosToolStripMenuItem";
            this.listadoDeRecibosToolStripMenuItem.Size = new System.Drawing.Size(117, 20);
            this.listadoDeRecibosToolStripMenuItem.Text = "Listado de Recibos";
            this.listadoDeRecibosToolStripMenuItem.Click += new System.EventHandler(this.listadoDeRecibosToolStripMenuItem_Click);
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(41, 20);
            this.salirToolStripMenuItem.Text = "Salir";
            this.salirToolStripMenuItem.Click += new System.EventHandler(this.salirToolStripMenuItem_Click);
            // 
            // Liquidador
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1419, 779);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Liquidador";
            this.Text = "Liquidador de Sueldos";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem aBMEmpleadoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aBMConceptoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem altaReciboToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listadoDeRecibosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
    }
}

