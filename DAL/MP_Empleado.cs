﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class MP_Empleado
    {
        private Acceso acceso = new Acceso();

        public List<BE.Empleado> Listar()
        {
            DataTable tabla = acceso.Leer("Empleado_Listar", null);
            List<BE.Empleado> empleados = new List<BE.Empleado>();

            foreach (DataRow registro in tabla.Rows)
            {
                BE.Empleado empleado = new BE.Empleado();

                empleado.Legajo = int.Parse(registro["Legajo"].ToString());
                empleado.Nombre = registro["Nombre"].ToString();
                empleado.Apellido = registro["Apellido"].ToString();
                empleado.Cuil = registro["Cuil"].ToString();
                empleado.Fec_alta = DateTime.Parse(registro["Fecha_Alta"].ToString());


                empleados.Add(empleado);
            }

            return empleados;
        }

        public int Insertar(BE.Empleado empleado)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametro("@nom", empleado.Nombre));
            parametros.Add(acceso.CrearParametro("@ape", empleado.Apellido));
            parametros.Add(acceso.CrearParametro("@cuil", empleado.Cuil));
            parametros.Add(acceso.CrearParametro("@fec_alta", empleado.Fec_alta));

            return acceso.Escribir("Empleado_Insertar", parametros);
        }

        public int Editar(BE.Empleado empleado)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametro("@leg", empleado.Legajo));
            parametros.Add(acceso.CrearParametro("@nom", empleado.Nombre));
            parametros.Add(acceso.CrearParametro("@ape", empleado.Apellido));
            parametros.Add(acceso.CrearParametro("@cuil", empleado.Cuil));
            parametros.Add(acceso.CrearParametro("@fec_alta", empleado.Fec_alta));

            return acceso.Escribir("Empleado_Editar", parametros);
        }

        public int Borrar(BE.Empleado empleado)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametro("@leg", empleado.Legajo));
            
            return acceso.Escribir("Empleado_Borrar", parametros);
        }
    }
}
