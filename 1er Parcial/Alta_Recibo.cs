﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _1er_Parcial
{
    public partial class Alta_Recibo : Form
    {
        public Alta_Recibo()
        {
            InitializeComponent();
            this.WindowState = FormWindowState.Maximized;
            Enlazar();
        }

        //BE.Recibo Rec = new BE.Recibo();
        BLL.Empleado BLLEmp = new BLL.Empleado();
        BLL.Concepto BLLCon = new BLL.Concepto();
        BLL.Recibo BLLRec = new BLL.Recibo();
        

        public void Enlazar()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = BLLEmp.Listar();
            dataGridView2.DataSource = null;
            dataGridView2.DataSource = BLLCon.Listar();
            dataGridView3.DataSource = null;
            dataGridView3.DataSource = BLLRec.Listar();
            
        }

        private void btnLiq_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtMex.Text) || string.IsNullOrWhiteSpace(txtanio.Text) || string.IsNullOrWhiteSpace(txtsbru.Text))
            {
                MessageBox.Show("Los Campos Mes, Anio y Sueldo Bruto deben contener datos válidos");
            }
            else
            {
                if(int.Parse(txtMex.Text) >= 1 && int.Parse(txtMex.Text) <= 12)
                {
                    BE.Recibo Rec = new BE.Recibo();
                    Rec.Legajo = int.Parse(dataGridView1.SelectedRows[0].Cells[0].Value.ToString());
                    Rec.Mes = int.Parse(txtMex.Text);
                    Rec.Anio = int.Parse(txtanio.Text);
                    Rec.Sdo_bruto = float.Parse(txtsbru.Text);

                    Rec.Tot_dtos = 0;
                    Rec.Sdo_neto = 0;
                    List<BE.Concepto> lista = BLLCon.Listar();
                    foreach (BE.Concepto concepto in lista)
                    {
                        if (concepto.Tipo_concepto == "Negativo")
                        {
                            Rec.Tot_dtos += Rec.Sdo_bruto * concepto.Num_calculo * -1;
                        }
                        else
                        {
                            Rec.Tot_dtos += Rec.Sdo_bruto * concepto.Num_calculo;
                        }
                    }

                    Rec.Sdo_neto = Rec.Sdo_bruto + Rec.Tot_dtos;
                    BLLRec.Insertar(Rec);
                    Enlazar();
                }
                else
                {
                    MessageBox.Show("El Mes debe contener valores entre 1 y 12");
                }
                
            }
            

        }

        private void btnSal_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void validarRecibo(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
              if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}
