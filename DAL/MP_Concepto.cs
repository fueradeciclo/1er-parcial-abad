﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class MP_Concepto
    {
        private Acceso acceso = new Acceso();

        public List<BE.Concepto> Listar()
        {
            DataTable tabla = acceso.Leer("Concepto_Listar", null);
            List<BE.Concepto> conceptos = new List<BE.Concepto>();

            foreach (DataRow registro in tabla.Rows)
            {
                BE.Concepto concepto = new BE.Concepto();

                concepto.Id_con = int.Parse(registro["ID_Concepto"].ToString());
                concepto.Descripcion = registro["Descripcion"].ToString();
                concepto.Porcentaje = float.Parse(registro["Porcentaje"].ToString());
                concepto.Num_calculo = float.Parse(registro["Num_Calculo"].ToString());
                concepto.Tipo_concepto = registro["Tipo_Con"].ToString();

                conceptos.Add(concepto);
            }

            return conceptos;
        }

        public int Insertar(BE.Concepto concepto)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametro("@des", concepto.Descripcion));
            parametros.Add(acceso.CrearParametro("@por", concepto.Porcentaje));
            parametros.Add(acceso.CrearParametro("@ncal", concepto.Num_calculo));
            parametros.Add(acceso.CrearParametro("@tip", concepto.Tipo_concepto));


            return acceso.Escribir("Concepto_Insertar", parametros);
        }

        public int Editar(BE.Concepto concepto)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametro("@ID", concepto.Id_con));
            parametros.Add(acceso.CrearParametro("@des", concepto.Descripcion));
            parametros.Add(acceso.CrearParametro("@por", concepto.Porcentaje));
            parametros.Add(acceso.CrearParametro("@ncal", concepto.Num_calculo));
            parametros.Add(acceso.CrearParametro("@tip", concepto.Tipo_concepto));

            return acceso.Escribir("Concepto_Editar", parametros);
        }

        public int Borrar(BE.Concepto concepto)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametro("@ID", concepto.Id_con));

            return acceso.Escribir("Concepto_Borrar", parametros);
        }

    }
}
