﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _1er_Parcial
{
    public enum Signo
    {
        Positivo,
        Negativo
    }

    public enum Mes
    {
        Enero,
        Febrero,
        Marzo,
        Abril,
        Mayo,
        Junio,
        Julio,
        Agosto,
        Septiembre,
        Octubre,
        Noviembre,
        Diciembre
    }
}